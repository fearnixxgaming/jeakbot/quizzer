package de.fearnixx.quizzer.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "Score")
@Table(name = "SCORE")
public class Score implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SCORE_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "QUIZ_ID")
    private Quiz quiz;

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "POINTS")
    private Integer points = 0;

    @Column(name = "QUESTIONS_CORRECT")
    private Integer questionsCorrect = 0;

    @Column(name = "QUESTIONS_INCORRECT")
    private Integer questionsIncorrect = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getQuestionsCorrect() {
        return questionsCorrect;
    }

    public void setQuestionsCorrect(Integer questionsCorrect) {
        this.questionsCorrect = questionsCorrect;
    }

    public Integer getQuestionsIncorrect() {
        return questionsIncorrect;
    }

    public void setQuestionsIncorrect(Integer questionsIncorrect) {
        this.questionsIncorrect = questionsIncorrect;
    }

    public void incrementQuestionsCorrect() {
        this.questionsCorrect++;
    }

    public void incrementQuestionsIncorrect() {
        this.questionsIncorrect++;
    }
}
