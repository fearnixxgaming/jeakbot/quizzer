package de.fearnixx.quizzer.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity(name = "Quiz")
@Table(name = "QUIZ")
public class Quiz implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "QUIZ_ID")
    private Integer id;

    @Column(name = "CREATOR_UUID")
    private String creatorUuid;

    @Column(name = "CREATED_TSP")
    private LocalDateTime createdTsp;

    @Column(name = "STARTED_TSP")
    private LocalDateTime startedTsp;

    @Column(name = "FINISHED_TSP")
    private LocalDateTime finishedTsp;

    @Column(name = "PARTICIPANTS")
    private Integer participants = 0;

    @Column(name = "QUESTIONS")
    private Integer questions;

    @Column(name = "STATE")
    private String state = "CREATED";

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatorUuid() {
        return creatorUuid;
    }

    public void setCreatorUuid(String creatorUuid) {
        this.creatorUuid = creatorUuid;
    }

    public LocalDateTime getCreatedTsp() {
        return createdTsp;
    }

    public void setCreatedTsp(LocalDateTime createdTsp) {
        this.createdTsp = createdTsp;
    }

    public LocalDateTime getStartedTsp() {
        return startedTsp;
    }

    public void setStartedTsp(LocalDateTime startedTsp) {
        this.startedTsp = startedTsp;
    }

    public LocalDateTime getFinishedTsp() {
        return finishedTsp;
    }

    public void setFinishedTsp(LocalDateTime finishedTsp) {
        this.finishedTsp = finishedTsp;
    }

    public Integer getParticipants() {
        return participants;
    }

    public void setParticipants(Integer participants) {
        this.participants = participants;
    }

    public Integer getQuestions() {
        return questions;
    }

    public void setQuestions(Integer questions) {
        this.questions = questions;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
