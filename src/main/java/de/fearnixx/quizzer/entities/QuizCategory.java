package de.fearnixx.quizzer.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity(name = "QuizCategory")
@Table(name = "QUIZ_CATEGORY")
public class QuizCategory implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "QUIZ_ID")
    private Quiz quiz;

    @Id
    @Column(name = "CATEGORY")
    private String category = "*";

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuizCategory that = (QuizCategory) o;
        return Objects.equals(quiz, that.quiz) &&
                Objects.equals(category, that.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quiz, category);
    }
}
