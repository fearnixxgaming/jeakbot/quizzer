package de.fearnixx.quizzer;

import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.*;
import de.fearnixx.jeak.service.command.ICommandReceiver;
import de.fearnixx.jeak.service.command.ICommandService;
import de.fearnixx.jeak.service.task.ITask;
import de.fearnixx.jeak.service.task.ITaskService;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.TargetType;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;
import de.fearnixx.jeak.util.Configurable;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@JeakBotPlugin(id = "quizzer")
public class Quizzer extends Configurable {

    private static final String DEFAULT_CONFIG_URI = "/quizzer/defaultConfig.json";

    private Map<String, Quiz> quizzes = new HashMap<>();
    private List<QuizQuestion> allQuestions = new ArrayList<>();

    private Map<String, ITask> quizTasks = new HashMap<>();

    @Inject
    private ICommandService commandService;

    @Inject
    private IInjectionService injectionService;

    @Inject
    private IServer server;

    @Inject
    private IDataCache cache;

    @Inject
    private ITaskService taskService;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Inject
    @PersistenceUnit(name = "quizzer")
    private EntityManager entityManager;

    public boolean isDatabaseConnected() {
        return databaseConnected;
    }

    private boolean databaseConnected;

    @Inject
    @Config
    private IConfig configRef;

    private ICommandReceiver commandReceiver = new QuizzerCommand(this);

    private int questionCount;
    private int questionDuration;

    private List<Integer> playWhitelist;
    private List<Integer> playBlacklist;
    private List<Integer> createWhitelist;
    private List<Integer> createBlacklist;

    private int maxQuizzes;

    public Quizzer() {
        super(Quizzer.class);
    }

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent initializeEvent) {
        injectionService.injectInto(commandReceiver);
        commandService.registerCommand("quizzer", commandReceiver);

        if (!loadConfig()) {
            initializeEvent.cancel();
        }

        questionCount = getConfig().getNode("questionCount").optInteger().orElse(5);
        questionDuration = getConfig().getNode("questionDuration").optInteger().orElse(30);
        maxQuizzes = getConfig().getNode("maxQuizzes").optInteger().orElse(20);

        initPermissions();

        loadQuestions();

        if (entityManager != null) {
            databaseConnected = true;

            entityManager.getTransaction().begin();

            //Close cancelled games
            TypedQuery<de.fearnixx.quizzer.entities.Quiz> brokenQuizzes =
                    entityManager.createQuery(
                            "SELECT q FROM Quiz q WHERE State = 'STARTED'",
                            de.fearnixx.quizzer.entities.Quiz.class
                    );

            brokenQuizzes.getResultList().forEach(quiz -> {
                quiz.setState("CANCELLED");
                entityManager.persist(quiz);
            });

            entityManager.flush();
            entityManager.getTransaction().commit();
        }
    }

    private void initPermissions() {
        IConfigNode permissionNode = getConfig().getNode("permissions");

        playWhitelist = permissionNode.getNode("playWhitelist")
                .optList()
                .orElseGet(ArrayList::new)
                .stream().map(IConfigNode::asInteger)
                .collect(Collectors.toList());

        playBlacklist = permissionNode.getNode("playBlacklist")
                .optList()
                .orElseGet(ArrayList::new)
                .stream().map(IConfigNode::asInteger)
                .collect(Collectors.toList());

        createWhitelist = permissionNode.getNode("createWhitelist")
                .optList()
                .orElseGet(ArrayList::new)
                .stream().map(IConfigNode::asInteger)
                .collect(Collectors.toList());

        createBlacklist = permissionNode.getNode("createBlacklist")
                .optList()
                .orElseGet(ArrayList::new)
                .stream().map(IConfigNode::asInteger)
                .collect(Collectors.toList());
    }

    private void loadQuestions() {
        getConfig().getNode("questions").asList().forEach(questionNode -> {

            String question = questionNode.getNode("question").asString();

            List<String> answers = questionNode.getNode("answers")
                    .asList().stream().map(IConfigNode::asString).collect(Collectors.toList());

            List<String> categories = questionNode.getNode("categories")
                    .optList().orElseGet(ArrayList::new).stream().map(IConfigNode::asString).collect(Collectors.toList());

            int points = questionNode.getNode("points").optInteger(1);

            int type = questionNode.getNode("type").optInteger(0);

            allQuestions.add(
                    new QuizQuestion(question, answers, categories, points, type)
            );
        });
    }

    @Override
    protected void onDefaultConfigLoaded() {
        saveConfig();
    }

    @Override
    protected IConfig getConfigRef() {
        return configRef;
    }

    @Override
    protected String getDefaultResource() {
        return DEFAULT_CONFIG_URI;
    }

    @Override
    protected boolean populateDefaultConf(IConfigNode iConfigNode) {
        return false;
    }

    @Override
    public IConfigNode getConfig() {
        return configRef.getRoot();
    }

    public Set<String> getUsedIDs() {
        return Collections.unmodifiableSet(quizzes.keySet());
    }

    public boolean createQuiz(String id, IClient creator, List<String> categories) {
        if (quizzes.containsKey(id) || maxQuizAmountReached()) {
            return false;
        }

        int quizDatabaseId = 0;

        if (databaseConnected) {
            entityManager.getTransaction().begin();

            de.fearnixx.quizzer.entities.Quiz q = new de.fearnixx.quizzer.entities.Quiz();
            q.setCreatorUuid(creator.getClientUniqueID());
            q.setCreatedTsp(LocalDateTime.now());

            entityManager.persist(q);

            quizDatabaseId = q.getId();

            entityManager.flush();
            entityManager.getTransaction().commit();
        }

        Quiz quiz = new Quiz(this, id, creator, questionCount, questionDuration, categories, quizDatabaseId);
        quiz.setDuration(questionDuration);

        quizzes.put(id, quiz);

        return true;
    }

    public Optional<Quiz> getQuizById(String id) {
        return Optional.ofNullable(quizzes.get(id));
    }

    public Optional<Quiz> getQuizByClientUid(String clientUid) {
        return quizzes.values().stream().filter(q -> q.getParticipants().contains(clientUid)).findFirst();
    }

    public boolean isClientParticipating(IClient client) {
        return quizzes.values()
                .stream()
                .anyMatch(quiz -> quiz.getParticipants().contains(client.getClientUniqueID()));
    }

    public void scheduleTask(Runnable task, int duration, String quizId) {
        ITask quizTask = ITask.builder()
                .runnable(() -> {
                    task.run();
                    quizTasks.remove(quizId);
                })
                .name(quizId).delay(duration, TimeUnit.SECONDS).build();
        quizTasks.put(quizId, quizTask);
        taskService.scheduleTask(quizTask);
    }

    public void sendTextMessageToClient(IClient target, String message) {
        sendTextMessageToClient(target.getClientUniqueID(), message);
    }

    public void sendTextMessageToClient(String targetUID, String message) {
        cache.findClientByUniqueId(targetUID).ifPresent(client -> {

            IQueryRequest request = IQueryRequest.builder()
                    .command("sendtextmessage")
                    .addKey(PropertyKeys.TextMessage.TARGET_TYPE, TargetType.CLIENT.getQueryNum())
                    .addKey(PropertyKeys.TextMessage.TARGET_ID, client.getClientID())
                    .addKey(PropertyKeys.TextMessage.MESSAGE, message)
                    .build();

            server.getConnection().sendRequest(request);
        });
    }

    public void sendTextMessageToAllQuizMembers(String quizId, String message, boolean notifyCreator) {
        Optional<Quiz> q = getQuizById(quizId);

        if (!q.isPresent()) {
            throw new IllegalArgumentException("Invalid quiz ID!");
        }

        q.get()
                .getParticipants()
                .forEach(
                        clientUid -> sendTextMessageToClient(clientUid, message)
                );

        if (notifyCreator && !q.get().getParticipants().contains(q.get().getCreatorUID())) {
            sendTextMessageToClient(q.get().getCreatorUID(), message);
        }
    }

    public void removeQuiz(String id) {
        quizzes.remove(id);

        if (quizTasks.containsKey(id)) {
            taskService.removeTask(quizTasks.get(id));
            quizTasks.remove(id);
        }
    }

    public void refreshQuestions() {
        allQuestions.clear();
        loadConfig();

        loadQuestions();
    }

    public String getClientNickname(String clientUid) {
        Optional<IClient> client = cache.findClientByUniqueId(clientUid);

        if (!client.isPresent()) {
            throw new IllegalArgumentException("Could not get clients nickname");
        }

        return client.get().getNickName();
    }

    public void removeQuizTask(String id) {
        if (quizTasks.containsKey(id)) {
            taskService.removeTask(quizTasks.get(id));
            quizTasks.remove(id);
        }
    }

    public List<QuizQuestion> supplyQuestions(int questionCount, List<String> categories) {
        List<QuizQuestion> questions = allQuestions
                .stream()
                .filter(question -> question.isOneOfCatagories(categories))
                .collect(Collectors.toList());

        Collections.shuffle(questions);
        return questions.stream().limit(questionCount).collect(Collectors.toList());
    }

    public boolean canCreate(List<Integer> groupIds) {

        if (groupIds.stream().anyMatch(createBlacklist::contains)) {
            return false;
        }

        return createWhitelist.isEmpty() || groupIds.stream().anyMatch(createWhitelist::contains);
    }

    public boolean canPlay(List<Integer> groupIds) {

        if (groupIds.stream().anyMatch(playBlacklist::contains)) {
            return false;
        }

        return playWhitelist.isEmpty() || groupIds.stream().anyMatch(playWhitelist::contains);
    }

    public boolean maxQuizAmountReached() {
        return quizzes.size() >= maxQuizzes;
    }

    public void listQuizzes(IClient client) {
        StringBuilder quizzesOutput = new StringBuilder("\nQUIZ-ID   :   #PARTICIPANTS");

        quizzes.forEach(
                (id, quiz) ->
                        quizzesOutput
                                .append("\n")
                                .append(id).append("   :   ")
                                .append(quiz.getParticipants().size())
        );

        sendTextMessageToClient(client, quizzesOutput.toString());
    }
}
