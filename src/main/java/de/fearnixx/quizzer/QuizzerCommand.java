package de.fearnixx.quizzer;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.ICommandContext;
import de.fearnixx.jeak.service.command.ICommandReceiver;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class QuizzerCommand implements ICommandReceiver {

    @Inject
    private IDataCache cache;

    private Quizzer plugin;

    public QuizzerCommand(Quizzer plugin) {
        this.plugin = plugin;
    }

    @Override
    public void receive(ICommandContext ctx) throws CommandException {
        if (ctx.getArguments().isEmpty()) {
            throw new CommandException("An argument is required!");
        }

        switch (ctx.getArguments().get(0)) {
            case "create":
                createQuiz(ctx, false);
                break;
            case "join":
                joinQuiz(ctx);
                break;
            case "create-join":
                createQuiz(ctx, true);
                break;
            case "leave":
                leaveQuiz(ctx);
                break;
            case "list":
                listQuizzes(ctx);
                break;
            case "param":
                adjustQuiz(ctx);
                break;
            case "start":
                startQuiz(ctx);
                break;
            case "stop":
                stopQuiz(ctx);
                break;
            case "refresh":
                refreshQuestions(ctx);
                break;
            case "answer":
                answer(ctx);
                break;
            default:
                throw new CommandException(
                        "Invalid first argument. " +
                                "Choose from 'create', 'join', 'start', 'stop', 'refresh'"
                );
        }
    }

    private void createQuiz(ICommandContext ctx, boolean creatorJoins) throws CommandException {

        IClient creator = getClient(ctx);

        if (!plugin.canCreate(creator.getGroupIDs())) {
            throw new CommandException("You are not allowed to use this command!");
        }

        if (plugin.maxQuizAmountReached()) {
            throw new CommandException("There are too many active quizzes!");
        }

        if (ctx.getArguments().size() < 2) {
            throw new CommandException("Cannot create a quiz without an ID!");
        }

        String id = ctx.getArguments().get(1);

        if (checkIdInUse(id)) {
            throw new CommandException("The id '" + id + "' is already in use.");
        }

        List<String> categoryList = new ArrayList<>();

        if (ctx.getArguments().size() >= 3) {
            categoryList = Arrays.asList(ctx.getArguments().get(2).split(","));
        }

        if (!plugin.createQuiz(id, creator, categoryList)) {
            throw new CommandException("Could not create quiz!");
        }

        plugin.sendTextMessageToClient(creator, "Your quiz with id '" + id + "' was created successfully!");

        if (creatorJoins && !plugin.isClientParticipating(creator)) {
            Optional<Quiz> q = plugin.getQuizById(id);

            if (!q.isPresent()) {
                throw new CommandException("Quiz not found!");
            }

            q.get().addParticipant(creator);
            plugin.sendTextMessageToClient(
                    q.get().getCreatorUID(), "You joined your quiz with id '" + id + "'"
            );
        }
    }

    private void joinQuiz(ICommandContext ctx) throws CommandException {

        IClient newParticipant = getClient(ctx);

        if (!plugin.canPlay(newParticipant.getGroupIDs())) {
            throw new CommandException("You are not allowed to use this command!");
        }

        if (ctx.getArguments().size() < 2) {
            throw new CommandException("Enter a valid ID to join!");
        }

        String id = ctx.getArguments().get(1);
        Optional<Quiz> q = plugin.getQuizById(id);

        if (!q.isPresent()) {
            throw new CommandException("No quiz with this ID!");
        }

        if (q.get().isStarted()) {
            throw new CommandException("The quiz you wanted to join has already started!");
        }

        if (plugin.isClientParticipating(newParticipant)) {
            throw new CommandException("You're already participating in another quiz");
        }

        q.get().addParticipant(newParticipant);

        plugin.sendTextMessageToClient(newParticipant, "You joined the quiz with id '" + id + "'!");
        plugin.sendTextMessageToClient(
                q.get().getCreatorUID(),
                newParticipant.getNickName() + " joined your quiz with id '" + id + "'"
        );
    }

    private void leaveQuiz(ICommandContext ctx) throws CommandException {
        IClient participant = getClient(ctx);

        if (!plugin.isClientParticipating(participant)) {
            throw new CommandException("You're not participating in a quiz!");
        }

        Optional<Quiz> quiz = plugin.getQuizByClientUid(participant.getClientUniqueID());

        if (!quiz.isPresent()) {
            throw new CommandException("Could not determine quiz");
        }

        quiz.get().removeParticipant(participant);

        plugin.sendTextMessageToClient(participant, "You left the quiz.");
        plugin.sendTextMessageToClient(
                quiz.get().getCreatorUID(),
                participant.getNickName() + " left your quiz with id '" + quiz.get().getQuizId() + "'"
        );
    }

    private void listQuizzes(ICommandContext ctx) throws CommandException {

        IClient client = getClient(ctx);

        if (!plugin.canCreate(client.getGroupIDs())) {
            throw new CommandException("You are not allowed to use this command!");
        }

        plugin.listQuizzes(client);
    }

    private void adjustQuiz(ICommandContext ctx) throws CommandException {
        if (ctx.getArguments().size() < 2) {
            throw new CommandException("You have to enter an id!");
        }

        String id = ctx.getArguments().get(1);

        Optional<Quiz> q = plugin.getQuizById(id);

        if (!q.isPresent()) {
            throw new CommandException("No quiz with this id!");
        }

        Quiz quiz = q.get();
        IClient client = getClient(ctx);

        if (!quiz.getCreatorUID().equals(client.getClientUniqueID())) {
            throw new CommandException("You cannot modify a quiz you have not created!");
        }

        if (quiz.isStarted()) {
            throw new CommandException("The quiz has already started and cannot be modified!");
        }

        if (ctx.getArguments().size() < 4) {
            throw new CommandException("You must enter a valid parameter and a value!");
        }

        String value = ctx.getArguments().get(3);

        switch (ctx.getArguments().get(2)) {
            case "duration":
                try {
                    quiz.setDuration(Integer.parseInt(value));
                    plugin.sendTextMessageToClient(client, "Duration was updated successfully!");
                } catch (NumberFormatException e) {
                    throw new CommandException("Enter a valid integer!");
                }
                break;
            case "count":
                try {
                    int val = Integer.parseInt(value);

                    if (val <= 0) {
                        throw new NumberFormatException();
                    }

                    quiz.setQuestionCount(val);
                    plugin.sendTextMessageToClient(client, "Number of questions was updated successfully!");
                } catch (NumberFormatException e) {
                    throw new CommandException("Enter a valid positive integer!");
                }
                break;
            case "categories":
                try {
                    List<String> categoryList = new ArrayList<>();

                    if (!value.equals("clear")) {
                        categoryList = Arrays.asList(value.split(","));
                    }

                    quiz.setCategories(categoryList);
                    plugin.sendTextMessageToClient(client, "Categories were updated successfully!");
                } catch (NumberFormatException e) {
                    throw new CommandException("Enter a valid positive integer!");
                }
                break;
            default:
                throw new CommandException("Not a valid parameter! Available: 'duration', 'count', 'categories'");
        }
    }

    private void startQuiz(ICommandContext ctx) throws CommandException {
        if (ctx.getArguments().size() < 2) {
            throw new CommandException("Enter a valid ID to start the quiz!");
        }

        String id = ctx.getArguments().get(1);
        Optional<Quiz> quiz = plugin.getQuizById(id);

        if (!quiz.isPresent()) {
            throw new CommandException("No quiz with this ID!");
        }

        IClient client = getClient(ctx);

        if (!quiz.get().getCreatorUID().equals(client.getClientUniqueID())) {
            throw new CommandException(
                    "You can only start quizzes that were created by you. Contact the creator to start this quiz."
            );
        }

        quiz.get().start();
    }

    private void stopQuiz(ICommandContext ctx) throws CommandException {
        if (ctx.getArguments().size() < 2) {
            throw new CommandException("Enter a valid ID to stop the quiz!");
        }

        String id = ctx.getArguments().get(1);
        Optional<Quiz> quiz = plugin.getQuizById(id);

        if (!quiz.isPresent()) {
            throw new CommandException("No quiz with this ID!");
        }

        IClient client = getClient(ctx);

        if (!quiz.get().getCreatorUID().equals(client.getClientUniqueID())) {
            throw new CommandException(
                    "You can only stop quizzes that were created by you. Contact the creator to start this quiz."
            );
        }

        quiz.get().stop();
    }

    private void refreshQuestions(ICommandContext ctx) throws CommandException {
        plugin.refreshQuestions();

        plugin.sendTextMessageToClient(getClient(ctx), "Refreshing questions.");
    }

    private void answer(ICommandContext ctx) throws CommandException {
        if (ctx.getArguments().size() < 2) {
            throw new CommandException("You must enter an answer!");
        }

        String answer = ctx.getArguments().get(1);

        IClient client = getClient(ctx);

        if (!plugin.isClientParticipating(client)) {
            throw new CommandException("You are not participating in a quiz!");
        }

        Optional<Quiz> quiz = plugin.getQuizByClientUid(client.getClientUniqueID());

        if (!quiz.isPresent()) {
            throw new CommandException("Could not find your quiz.");
        }

        if (!quiz.get().canAnswer()) {
            throw new CommandException("You are not allowed to answer at the current state of the game.");
        }

        answer = answer.replace(',', '.');
        boolean floatingPointNumber = answer.contains(".");

        try {
            if (!floatingPointNumber) {
                Integer.parseInt(answer);
            } else {
                Float.parseFloat(answer);
            }
        } catch (NumberFormatException e) {
            throw new CommandException("Invalid answer. Enter a number!");
        }

        quiz.get().answer(client, answer, floatingPointNumber);
    }

    private IClient getClient(ICommandContext ctx) throws CommandException {
        Optional<String> clientId = ctx.getRawEvent().getProperty(PropertyKeys.TextMessage.SOURCE_ID);

        if (!clientId.isPresent()) {
            throw new CommandException("Could not determine your client ID!");
        }

        Optional<IClient> client = cache.getClients()
                .stream()
                .filter(c -> c.getClientID().toString().equals(clientId.get()))
                .findFirst();

        if (!client.isPresent()) {
            throw new CommandException("Could not find you in cache! Try again in a moment.");
        }

        return client.get();
    }

    private boolean checkIdInUse(String id) {
        return plugin.getUsedIDs().stream().anyMatch(usedID -> usedID.equals(id));
    }

}
