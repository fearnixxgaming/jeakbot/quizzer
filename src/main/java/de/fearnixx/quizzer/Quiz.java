package de.fearnixx.quizzer;

import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.quizzer.entities.QuizCategory;
import de.fearnixx.quizzer.entities.Score;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.*;

public class Quiz {

    private static final String MSG_QUIZ_ID_PREFIX = "The quiz with id '";
    private Map<String, Integer> participants;

    private List<QuizQuestion> questions;
    private int questionCount;

    private List<String> categories;

    private Quizzer plugin;

    private boolean started = false;
    private boolean canAnswer = false;

    private String quizId;

    private int questionIndex;

    private Map<String, String> clientAnswers = new HashMap<>();

    private Integer correctAnswerIndex;
    private Float correctAnswer;

    private String creatorUID;

    private QuizQuestion currentQuestion;

    private int currentQuestionType;
    private int duration;

    private EntityManager entityManager;
    private int quizDatabaseId;

    public Quiz(Quizzer plugin, String quizId, IClient creator, int questionCount, int duration, List<String> categories, int quizDatabaseId) {
        this.questionCount = questionCount;
        this.creatorUID = creator.getClientUniqueID();
        this.plugin = plugin;
        this.quizId = quizId;

        this.duration = duration;
        this.categories = categories;

        entityManager = plugin.getEntityManager();
        this.quizDatabaseId = quizDatabaseId;

        participants = new HashMap<>();
    }

    public Set<String> getParticipants() {
        return Collections.unmodifiableSet(participants.keySet());
    }

    public boolean isStarted() {
        return started;
    }

    public boolean canAnswer() {
        return canAnswer;
    }

    public String getCreatorUID() {
        return creatorUID;
    }

    public boolean isClientParticipating(IClient client) {
        return getParticipants().contains(client.getClientUniqueID());
    }

    public boolean addParticipant(IClient client) {
        if (!started) {
            participants.put(client.getClientUniqueID(), 0);
        }

        return !started;
    }

    public void removeParticipant(IClient client) {
        participants.remove(client.getClientUniqueID());

        if (started && participants.isEmpty()) {
            plugin.removeQuiz(quizId);
            plugin.sendTextMessageToClient(creatorUID, "Your quiz with id '" + quizId + "' was abandoned and therefore removed.");

            if (plugin.isDatabaseConnected()) {
                entityManager.getTransaction().begin();

                de.fearnixx.quizzer.entities.Quiz q = getQuizFromDatabase();
                q.setState("ABANDONED");

                entityManager.persist(q);
                entityManager.flush();
                entityManager.getTransaction().commit();
            }
        }
    }

    public void answer(IClient client, String answer, boolean floatingPoint) throws CommandException {
        String uid = client.getClientUniqueID();

        if (clientAnswers.containsKey(uid)) {
            throw new CommandException("You have already given an answer!");
        }

        if (floatingPoint && currentQuestion.getType() != 1) {
            throw new CommandException("This type of question does not permit floating point answers!");
        }

        if (getParticipants().contains(uid)) {
            clientAnswers.put(uid, answer);
        }

        plugin.sendTextMessageToClient(uid, "Your answer (" + answer + ") was submitted.");

        if (clientAnswers.size() == participants.values().size()) {
            plugin.removeQuizTask(quizId);
            nextQuestion();
        }
    }

    public void start() {

        boolean invalidState = false;

        if (!this.started && plugin.isDatabaseConnected()) {
            entityManager.getTransaction().begin();

            de.fearnixx.quizzer.entities.Quiz quiz = getQuizFromDatabase();

            if(!quiz.getState().equals("CREATED")){
                invalidState = true;
            }

            entityManager.flush();
            entityManager.getTransaction().commit();
        }

        if(this.started || invalidState){
            plugin.sendTextMessageToAllQuizMembers(
                    quizId,
                    MSG_QUIZ_ID_PREFIX + quizId + "' is not in a valid state to start.",
                    false
            );
            return;
        }


        categories = new ArrayList<>(new HashSet<>(categories));
        questions = plugin.supplyQuestions(questionCount, categories);

        questionCount = questions.size();

        started = true;
        plugin.sendTextMessageToAllQuizMembers(
                quizId,
                MSG_QUIZ_ID_PREFIX + quizId + "' has started. You have " + duration + " seconds to answer the upcoming questions!",
                true
        );

        if (plugin.isDatabaseConnected()) {
            entityManager.getTransaction().begin();

            de.fearnixx.quizzer.entities.Quiz quiz = getQuizFromDatabase();

            quiz.setState("STARTED");
            quiz.setStartedTsp(LocalDateTime.now());
            quiz.setParticipants(participants.size());
            quiz.setQuestions(questionCount);

            entityManager.persist(quiz);

            if (categories.isEmpty()) {
                QuizCategory quizCategory = new QuizCategory();
                quizCategory.setQuiz(getQuizFromDatabase());
                entityManager.persist(quizCategory);
            } else {
                categories.forEach(category -> {
                    QuizCategory quizCategory = new QuizCategory();
                    quizCategory.setQuiz(getQuizFromDatabase());
                    quizCategory.setCategory(category);
                    entityManager.persist(quizCategory);
                });
            }

            participants.keySet().forEach(p ->
            {
                Score score = new Score();
                score.setUuid(p);
                score.setQuiz(quiz);

                entityManager.persist(score);
            });

            entityManager.flush();
            entityManager.getTransaction().commit();
        }

        nextQuestion();
    }

    public void nextQuestion() {
        if (questionIndex > 0) {
            checkResults();
        }

        if (questionIndex >= questions.size()) {
            this.quizFinished();
            return;
        }

        clientAnswers.clear();
        currentQuestion = questions.get(questionIndex++);

        StringBuilder answerBuilder = new StringBuilder();
        List<String> answers = currentQuestion.getAnswers();

        currentQuestionType = currentQuestion.getType();

        if (currentQuestionType == 0) {

            for (int i = 0; i < answers.size(); i++) {

                if (answers.get(i).equals(currentQuestion.getCorrectAnswer())) {
                    correctAnswerIndex = i + 1;
                }

                answerBuilder.append(i + 1);
                answerBuilder.append(": ").append(answers.get(i));

                if (i < answers.size() - 1) {
                    answerBuilder.append("\n");
                }
            }
        } else if (currentQuestionType == 1) {
            answerBuilder.append("Guess the value!");
            correctAnswer = Float.parseFloat(currentQuestion.getCorrectAnswer());
        }

        plugin.sendTextMessageToAllQuizMembers(
                quizId,
                "Quiz '" + quizId + "': Question No. " + questionIndex + " of " + questionCount + "\n" +
                        currentQuestion.getQuestion() + "\n" +
                        answerBuilder.toString(),
                false
        );

        canAnswer = true;

        if (questionIndex >= questions.size()) {
            plugin.scheduleTask(this::quizFinished, duration, quizId);
        } else {
            plugin.scheduleTask(this::nextQuestion, duration, quizId);
        }
    }

    private void checkResults() {
        if (plugin.isDatabaseConnected()) {
            entityManager.getTransaction().begin();
        }

        if (currentQuestionType == 0) {

            clientAnswers.entrySet().stream().filter(e -> correctAnswerIndex.equals(Integer.parseInt(e.getValue()))).forEach(
                    entry ->
                    {
                        String key = entry.getKey();

                        participants.computeIfPresent(key, (uid, val) -> val + currentQuestion.getPoints());

                        if (plugin.isDatabaseConnected()) {
                            TypedQuery<Score> scoreQuery = entityManager.createQuery(
                                    "SELECT s FROM Score s WHERE quiz = :quizId AND uuid = :uuid",
                                    Score.class
                            );

                            scoreQuery.setParameter("quizId", getQuizFromDatabase());
                            scoreQuery.setParameter("uuid", key);

                            Score score = scoreQuery.getSingleResult();

                            score.incrementQuestionsCorrect();
                            score.setPoints(participants.get(key));

                            entityManager.persist(score);
                        }
                    }
            );

            if (plugin.isDatabaseConnected()) {
                clientAnswers.entrySet().stream().filter(e -> !correctAnswerIndex.equals(Integer.parseInt(e.getValue()))).forEach(
                        entry ->
                        {
                            String key = entry.getKey();

                            TypedQuery<Score> scoreQuery = entityManager.createQuery(
                                    "SELECT s FROM Score s WHERE quiz = :quizId AND uuid = :uuid",
                                    Score.class
                            );

                            scoreQuery.setParameter("quizId", getQuizFromDatabase());
                            scoreQuery.setParameter("uuid", key);

                            Score score = scoreQuery.getSingleResult();
                            score.incrementQuestionsIncorrect();

                            entityManager.persist(score);
                        }
                );
            }

        } else if (currentQuestionType == 1) {
            String uid = "";

            float bestDiff = Float.MAX_VALUE;

            for (Map.Entry<String, String> answer : clientAnswers.entrySet()) {
                float diff = Math.abs(Float.parseFloat(answer.getValue()) - correctAnswer);

                if (diff < bestDiff) {
                    uid = answer.getKey();
                    bestDiff = diff;
                }
            }

            participants.computeIfPresent(uid, (cUid, val) -> val + currentQuestion.getPoints());

            if (plugin.isDatabaseConnected()) {
                final String winnerUid = uid;

                TypedQuery<Score> scoreQuery = entityManager.createQuery(
                        "SELECT s FROM Score s WHERE quiz = :quizId",
                        Score.class
                );

                scoreQuery.setParameter("quizId", getQuizFromDatabase());

                scoreQuery.getResultList().forEach(score -> {
                    if (score.getUuid().equals(winnerUid)) {
                        score.incrementQuestionsCorrect();
                        score.setPoints(participants.get(winnerUid));
                    } else {
                        score.incrementQuestionsIncorrect();
                    }

                    entityManager.persist(score);
                });
            }
        }

        if (plugin.isDatabaseConnected()) {
            entityManager.flush();
            entityManager.getTransaction().commit();
        }
    }


    private void quizFinished() {
        if (plugin.isDatabaseConnected()) {
            entityManager.getTransaction().begin();

            de.fearnixx.quizzer.entities.Quiz quiz = getQuizFromDatabase();
            quiz.setState("FINISHED");
            quiz.setFinishedTsp(LocalDateTime.now());

            entityManager.flush();
            entityManager.getTransaction().commit();
        }

        plugin.sendTextMessageToAllQuizMembers(quizId, MSG_QUIZ_ID_PREFIX + quizId + "' has ended.", true);
        plugin.sendTextMessageToAllQuizMembers(quizId, getScoreOutput(), false);

        plugin.sendTextMessageToClient(creatorUID, "Your quiz with id '" + quizId + "' will be removed now.");
        plugin.removeQuiz(quizId);
    }

    private String getScoreOutput() {
        StringBuilder scoreBuilder = new StringBuilder("\n");

        List<Map.Entry<String, Integer>> entries = new ArrayList<>(participants.entrySet());
        entries.sort(Map.Entry.comparingByValue(Comparator.comparingInt(o -> o)));
        Collections.reverse(entries);

        int index = 1;

        StringBuilder line;
        for (Map.Entry<String, Integer> entry : entries) {
            line = new StringBuilder();

            line.append(index);
            line.append(". ");
            line.append(plugin.getClientNickname(entry.getKey()));

            for (int i = line.toString().length(); i < 50; i++) {
                line.append(" ");
            }
            line.append(entry.getValue());
            line.append("\n");

            scoreBuilder.append(line);

            index++;
        }

        return scoreBuilder.toString();
    }

    public void stop() {
        if (plugin.isDatabaseConnected()) {
            entityManager.getTransaction().begin();

            getQuizFromDatabase().setState("STOPPED");

            entityManager.flush();
            entityManager.getTransaction().commit();
        }

        plugin.sendTextMessageToAllQuizMembers(
                quizId,
                MSG_QUIZ_ID_PREFIX + quizId + "' was stopped by the creator.",
                true
        );
        plugin.removeQuiz(quizId);
    }

    public String getQuizId() {
        return quizId;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setQuestionCount(int questionCount) {
        this.questionCount = questionCount;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    private de.fearnixx.quizzer.entities.Quiz getQuizFromDatabase() {
        TypedQuery<de.fearnixx.quizzer.entities.Quiz> q =
                entityManager.createQuery(
                        "SELECT q FROM Quiz q WHERE id = :quizId",
                        de.fearnixx.quizzer.entities.Quiz.class
                );

        q.setParameter("quizId", quizDatabaseId);

        return q.getSingleResult();
    }
}
