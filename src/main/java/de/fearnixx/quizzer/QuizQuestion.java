package de.fearnixx.quizzer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuizQuestion {

    private final String question;
    private final List<String> answers;

    private final String correctAnswer;

    private final int type;

    private final List<String> categories;

    private final int points;

    public QuizQuestion(String question, List<String> answers, List<String> categories, int points, int type) {
        if (question == null || question.isEmpty() || answers == null || answers.isEmpty()) {
            throw new IllegalArgumentException("Invalid question parameters.");
        }

        this.question = question;
        this.answers = answers;
        this.categories = categories;
        this.points = points;
        this.type = type;

        correctAnswer = answers.get(0);
    }

    public boolean isOneOfCatagories(List<String> categories) {
        return categories.isEmpty() || categories.stream().anyMatch(this::isCategory);
    }

    public boolean isCategory(String category) {
        return categories.stream().anyMatch(c -> c.equalsIgnoreCase(category));
    }

    public List<String> getAnswers() {
        List<String> shuffledAnswers = new ArrayList<>(answers);
        Collections.shuffle(shuffledAnswers);
        return shuffledAnswers;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public String getQuestion() {
        return question;
    }

    public int getType() {
        return type;
    }

    public int getPoints() {
        return points;
    }
}
