## **Quizzer-Plugin**

### **Introduction**

As the name suggests, Quizzer is a plugin to create and play quizzes within the *JeakBot-Framework*. 

You can configure as many different questions as you want. Additionally you can categorize them, assign different amount of points, create various amounts of possible answers and use different types of questions (currently two).

### **How to play**
To create and join lobbys to play quizzes one has to send commands to the **visible** *Jeak-Bot*.

##### 1. Create a lobby (**create**)

Any client can create a lobby with a specified ID by using the *create*-command. If only questions of specific categories should be selected, one can add the optional parameter and specify all categories by seperating them with a comma.

**Usage**: `!quizzer create lobbyID [categories]`


##### 2. Join an existing lobby (**join**)  
  
Any client can join a lobby with a specified ID by using the *join*-command  

**Usage**: `!quizzer join lobbyID`  
*Note*: A client can not join quizzes that have been started.

##### *Hint* : 
The creator of a lobby doesn't have to be in the quiz he created. If you want to instantly join your created lobby, use the command *create-join* which combines the two commands above. 

**Usage**: `!quizzer create-join lobbyID`


##### 3. Optional:  Adjust settings of existing lobby (**param**)
    
The creator of a lobby with a specified ID can adjust the duration of the time to answer a question, the selected categories and the number of questions.

**Usage**: 
    
    !quizzer param lobbyID duration   durationOfQuestions
    !quizzer param lobbyID count      numberOfQuestions
    !quizzer param lobbyID categories categories
    
##### 4. Start a quiz in a lobby (**start**)
    
The creator of a lobby with a specified ID can start the quiz by using the *start*-command.

**Usage**: `!quizzer start lobbyID`

##### 5. Give an answer in a quiz (**answer**)
    
Any participant is allowed to give one answer to the current question by using the *answer*-command. When every player has answered the question or the duration set by the creator has run out, either the next question or the results will show up.

The answer the client has to give, depends on the type of the current question. Usually one wants to select the index of the answer. If the question is a guessing question (numercial values only), the client just answers with his guess.

**Usage**: `!quizzer answer theAnswer`

##### Additional commands

By using `!quizzer leave` a client can leave the lobby (or the ongoing quiz if it has been started). *Note*: You will **not** be able to re-join an ongoing quiz!

The creator of a lobby can stop the quiz/lobby with a specified ID by using the stop-command  `!quizzer stop lobbyID`.

### **Questions + Config**

Below you will find the default config of the Quizzer-Plugin (*All default values are included*):

    {
    "questionCount": 5,
    "questionDuration": 30,
    "maxQuizzes": 20,
    "permissions": {
        "playWhitelist": [],
        "playBlacklist": [],
        "createWhitelist": [],
        "createBlacklist": []
      },
    "questions": [
        {
        "question": "Test-Question",
        "answers": [
            "This is the correct answer",
            "This answer is not correct",
            "This answer is not correct"
        ],
        "categories": [
            "default"
        ],
        "type": 0,
        "points": 1
        }
     ]
    }

#### **General config**

The general config parameters `questionCount` and `questionDuration` set the default values for a new lobby (see **param** to adjust those values for any lobby).

`maxQuizzes` contains the limit of active quizzes. In the node `permissions` a black- and whitelist can be specified for either creating or joining a quiz.

#### **Questions**  

The template above includes all information that can be set for a question. Some of them are not required and have defaults.

**question**
The question that will be asked by the bot. *Required*, *Must be not empty*.

**answers**
The possible answers, the bot will supply to the participants. The first answer in the list has to be the correct one. *Required*, *Must not be empty*.

**categories**
The categories of this question. *Not required*, *Can be empty*, Default: empty.

**type**
The type of the question where 0 is a classic indexed question (Select an answer from 1 to n, all correct answers win) and 1 is a guessing question (Guess the value, the closest answer wins). *Not required*, Default: 0.
*Note*: Guessing questions should just use one single answer.

**points**
The number of points the player with the correct answer will earn. *Not required*, Default: 1.